#include <stdint.h>

#define   DEFAULT 9
#define     BLACK 0
#define       RED 1
#define     GREEN 2
#define    YELLOW 3
#define      BLUE 4
#define   MAGENTA 5
#define      CYAN 6
#define    L_GRAY 7
#define    D_GRAY 60
#define     L_RED 61
#define   L_GREEN 62
#define  L_YELLOW 63
#define    L_BLUE 64
#define L_MAGENTA 65
#define    L_CYAN 66
#define     WHITE 67
