#include "format.h"
#include "constants/symbols.h"
#include "constants/numeric.h"

#include <stdbool.h>

#define BEGIN_SEPARATOR BEGIN_FORMAT BEGIN_SYMBOL

#define USER_MODULE USER_ICON_FORMAT USER_ICON USER_FORMAT "\\u " SEPARATOR " "
#define ROOT_MODULE ROOT_ICON_FORMAT ROOT_ICON ROOT_FORMAT "\\u " SEPARATOR " "
#define HOST_MODULE HOST_ICON_FORMAT HOST_ICON HOST_FORMAT "\\h " SEPARATOR " "
#define  GIT_MODULE  GIT_ICON_FORMAT  GIT_ICON  GIT_FORMAT "%s "  SEPARATOR " "
#define  PWD_MODULE  PWD_ICON_FORMAT "%s"       PWD_FORMAT "%s "

#define ERROR_MODULE ERROR_ICON_FORMAT "%s" ERROR_FORMAT "%i "

#define SLASH_REPLACER " " SEPARATOR " "

#define END_SEPARATOR_SUCCESS SUCCESS_FORMAT_1 END_SYMBOL \
	                          SUCCESS_FORMAT_2 END_SYMBOL \
	                          SUCCESS_FORMAT_3 END_SYMBOL
#define    END_SEPARATOR_FAIL FAIL_FORMAT_1 END_SYMBOL \
	                          FAIL_FORMAT_2 END_SYMBOL \
	                          FAIL_FORMAT_3 END_SYMBOL
#define CONT_SEPARATOR_SUCCESS CONT_SUCCESS_FORMAT_1 BEGIN_SYMBOL \
	                           SUCCESS_FORMAT_2 END_SYMBOL \
	                           SUCCESS_FORMAT_3 END_SYMBOL
#define    CONT_SEPARATOR_FAIL CONT_FAIL_FORMAT_1 BEGIN_SYMBOL \
	                           FAIL_FORMAT_2 END_SYMBOL \
	                           FAIL_FORMAT_3 END_SYMBOL
