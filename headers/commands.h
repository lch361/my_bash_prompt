#define GIT_BRANCH_COMMAND "git branch --show-current 2> /dev/null"
// format this with pwd
#define STAT_COMMAND "stat -c %%U \"%s\""

extern void get_output_of_command(const char *command, char *output);
