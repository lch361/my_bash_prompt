#include <boost/preprocessor/arithmetic/add.hpp>

#include "constants/colors.h"
#include "constants/styles.h"
#include "constants/color_types.h"

#define xstr(s) str(s)
#define str(s) #s

#define _font_format(_fg, _bg, _fe) "\\[\\033[0;" \
xstr(BOOST_PP_ADD(_fg, COLOR )) ";" \
xstr(BOOST_PP_ADD(_bg, BG    )) ";" \
xstr(BOOST_PP_ADD(_fe, EFFECT)) "m\\]"

#define BEGIN_FORMAT _font_format(BLACK  , DEFAULT, BOLD)

#define  USER_FORMAT _font_format(MAGENTA, BLACK  , BOLD)
#define  ROOT_FORMAT _font_format(CYAN   , BLACK  , BOLD)
#define  HOST_FORMAT _font_format(YELLOW , BLACK  , BOLD)
#define   GIT_FORMAT _font_format(GREEN  , BLACK  , BOLD)
#define   PWD_FORMAT _font_format(BLUE   , BLACK  , BOLD)
#define ERROR_FORMAT _font_format(RED    , BLACK  , BOLD)

#define  USER_ICON_FORMAT _font_format(L_MAGENTA, BLACK  , BOLD)
#define  ROOT_ICON_FORMAT _font_format(L_CYAN   , BLACK  , BOLD)
#define  HOST_ICON_FORMAT _font_format(L_YELLOW , BLACK  , BOLD)
#define   GIT_ICON_FORMAT _font_format(L_GREEN  , BLACK  , BOLD)
#define   PWD_ICON_FORMAT _font_format(L_BLUE   , BLACK  , BOLD)
#define ERROR_ICON_FORMAT _font_format(L_RED    , BLACK  , BOLD)

#define NO_FORMAT "\\[\\033[0m\\]"

#define PROMPT_FORMAT _font_format(DEFAULT, DEFAULT, BOLD)

#define CONT_SUCCESS _font_format(BLUE, DEFAULT, BOLD)
#define    CONT_FAIL _font_format(RED , DEFAULT, BOLD)

#define SUCCESS_FORMAT_1 _font_format(BLACK , BLUE   , BOLD)
#define SUCCESS_FORMAT_2 _font_format(BLUE  , L_BLUE , BOLD)
#define SUCCESS_FORMAT_3 _font_format(L_BLUE, DEFAULT, BOLD)

#define    FAIL_FORMAT_1 _font_format(BLACK, RED    , BOLD)
#define    FAIL_FORMAT_2 _font_format(RED  , L_RED  , BOLD)
#define    FAIL_FORMAT_3 _font_format(L_RED, DEFAULT, BOLD)

#define CONT_SUCCESS_FORMAT_1 _font_format(BLUE, DEFAULT, BOLD)
#define    CONT_FAIL_FORMAT_1 _font_format(RED , DEFAULT, BOLD)
