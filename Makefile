CC=gcc
CFLAGS=-Wall -I$(HEADERS) -I/usr/local/include/boost_1_81_0

SOURCES=src
HEADERS=headers
TESTS=test
TARGET=target
TEST=$(TARGET)/test
RELEASE=$(TARGET)/release
DEBUG=$(TARGET)/debug

OUTPUT=$(shell basename -- $(shell pwd))

all: debug

debug:
	mkdir -p $(DEBUG)
	$(CC) $(CFLAGS) -g -o $(DEBUG)/$(OUTPUT) $(SOURCES)/*

release:
	mkdir -p $(RELEASE)
	$(CC) $(CFLAGS) -O2 -o $(RELEASE)/$(OUTPUT) $(SOURCES)/*

test:
	mkdir -p $(TEST)
	$(CC) $(CFLAGS) -g -o $(TEST)/$(OUTPUT) \
	$(shell find $(SOURCES) -type f ! -name main.*) $(TESTS)/main.*

clean:
	rm -rf $(TARGET)

install:
	cp $(RELEASE)/$(OUTPUT) ~/.local/bin/
