#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>

#include "commands.h"
#include "print_modules.h"

#define BUF 30

int main(int argc, const char **argv) {
	const int  exit_code = argc < 2 ? 0 : strtol(argv[1], NULL, 10);
	const bool exit_success = exit_code == 0;
	const char *pwd  = getenv("PWD");
	const char *home = getenv("HOME");
	const char *user = getenv("USER");

	size_t pwd_len  = strlen(pwd);
	size_t home_len = strlen(home);

	char git_branch[BUF] = "";
	get_output_of_command(GIT_BRANCH_COMMAND, git_branch);

	char dir_owner[BUF] = "";
	char *stat_command = malloc(sizeof(STAT_COMMAND) + pwd_len);
	sprintf(stat_command, STAT_COMMAND, pwd);
	get_output_of_command(stat_command, dir_owner);
	free(stat_command);

	bool is_home = false;
	if (strstr(pwd, home) == pwd) {
		is_home = true;
		pwd += home_len;
		pwd_len -= home_len;
	}
	bool is_truncated = (pwd_len + is_home) > PWD_MAX_LENGTH;
	if (is_truncated) {
		pwd += pwd_len - PWD_MAX_LENGTH;
	}
	
	char pwd_module[PWD_MAX_LENGTH * 3 + sizeof(PWD_MODULE)] = {};
	char *pwd_module_ptr = pwd_module;
	if (is_home && !is_truncated) {
		for (const char *j = HOME_DIR_SYMBOL; *j != '\0'; ++j) {
			*pwd_module_ptr++ = *j;
		}
	}
	if (is_truncated) {
		for (const char *j = PWD_TRUNC_SYMBOL; *j != '\0'; ++j) {
			*pwd_module_ptr++ = *j;
		}
	}
	for (const char *i = pwd; *i != '\0'; ++i) {
		if (*i != '/') {
			*pwd_module_ptr++ = *i;
		}
		else {
			for (const char *j = SLASH_REPLACER; *j != '\0'; ++j) {
				*pwd_module_ptr++ = *j;
			}
		}
	}

	printf("%s%s%s",
		BEGIN_SEPARATOR,
		(strcmp(home, "/root") == 0) ? ROOT_MODULE : USER_MODULE,
		HOST_MODULE);

	if (git_branch[0] != '\0') {
		printf(GIT_MODULE, git_branch);
	}

	printf(PWD_MODULE,
		(strcmp(dir_owner, user) == 0) ? FOLDER_UNLOCKED : FOLDER_LOCKED,
		pwd_module);

	if (!exit_success) {
		printf(ERROR_MODULE, ERROR_ICON, exit_code);
	}

	printf("%s%s \n",
			exit_success ? END_SEPARATOR_SUCCESS : END_SEPARATOR_FAIL,
			PROMPT_FORMAT);

	printf("%s%s \n",
		exit_success ? CONT_SEPARATOR_SUCCESS : CONT_SEPARATOR_FAIL,
		PROMPT_FORMAT);
}
