#include "commands.h"

#include <stdlib.h>
#include <stdio.h>

#define LINE_BUFFER 240

void get_output_of_command(const char *command, char *output) {
	FILE *process = popen(command, "r");
	if (process == NULL) return;

	fgets(output, LINE_BUFFER, process);
	for (char *i = output; *i != '\0'; ++i) {
		if (*i == '\n') *i = '\0';
	}
	pclose(process);
}
